# GWANW22_LIGO_tutorials

This is a repo to store and organize notebooks and links for 2022 GWANW student workshop on LIGO data and analyses. The LIGO sections will consist of 3 tutorial sessions covering LIGO data access and maniupulation (Adrian Helmling-Cornell), GW source parameter estimation using [bilby](https://git.ligo.org/lscsoft/bilby) (Alan Knee) and a population/rate analysis of a catalog of sources using [GWpopulation](https://github.com/ColmTalbot/gwpopulation) (Bruce Edelman). 

Date of Workshop: Monday June 27th 10am to 5pm PDT at the LIGO Hanford Observatory (LHO) and on Zoom

## Contacts:
* Data Tutorial: Adrian Helmling-Cornell (University of Oregon -- ahelmlin@uoregon.edu)
* Bilby Tutorial: Alan Knee (University of British Columbia -- aknee@phas.ubc.ca)
* GWPopulation Tutorial: Bruce Edelman (University of Oregon -- bedelman@uoregon.edu)

## Notebook Tutorial Links 
* Data Tutorial - [Gitlab](https://git.ligo.org/bruce.edelman/gwanw22_ligo_tutorials/-/blob/main/basic_data_access/gwanw_final_version.ipynb), [Colab]()
* PE Tutorial - [Gitlab](https://git.ligo.org/bruce.edelman/gwanw22_ligo_tutorials/-/blob/main/pe_tutorial/gwanw22_bilby_tutorial.ipynb), [Colab]()
* Pop Tutorial - [Gitlab](https://git.ligo.org/bruce.edelman/gwanw22_ligo_tutorials/-/blob/main/population_tutorial/gwpopulation_tutorial.ipynb), [Colab](https://colab.research.google.com/drive/1hyTcIuSpr3IkvC1EH9D7R92B8Yie72_I?usp=sharing )
