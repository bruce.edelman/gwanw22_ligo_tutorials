# packages in environment at /home/adrian.helmling-cornell/.conda/envs/GWANW_env:
#
# Name                    Version                   Build  Channel
_libgcc_mutex             0.1                 conda_forge    conda-forge
_openmp_mutex             4.5                       2_gnu    conda-forge
astropy                   5.1             py310hde88566_0    conda-forge
asttokens                 2.0.5                    pypi_0    pypi
backcall                  0.2.0                    pypi_0    pypi
boost-cpp                 1.74.0               h75c5d50_8    conda-forge
brotli                    1.0.9                h166bdaf_7    conda-forge
brotli-bin                1.0.9                h166bdaf_7    conda-forge
brotlipy                  0.7.0           py310h5764c6d_1004    conda-forge
bzip2                     1.0.8                h7f98852_4    conda-forge
c-ares                    1.18.1               h7f98852_0    conda-forge
ca-certificates           2022.6.15            ha878542_0    conda-forge
cached-property           1.5.2                hd8ed1ab_1    conda-forge
cached_property           1.5.2              pyha770c72_1    conda-forge
certifi                   2022.6.15       py310hff52083_0    conda-forge
cffi                      1.15.0          py310h0fdd8cc_0    conda-forge
charset-normalizer        2.0.12             pyhd8ed1ab_0    conda-forge
colorama                  0.4.5              pyhd8ed1ab_0    conda-forge
cryptography              37.0.2          py310h597c629_0    conda-forge
cycler                    0.11.0             pyhd8ed1ab_0    conda-forge
debugpy                   1.6.0                    pypi_0    pypi
decorator                 5.1.1                    pypi_0    pypi
dqsegdb2                  1.1.1              pyhd8ed1ab_0    conda-forge
entrypoints               0.4                      pypi_0    pypi
executing                 0.8.3                    pypi_0    pypi
fftw                      3.3.10          nompi_h77c792f_102    conda-forge
fonttools                 4.33.3          py310h5764c6d_0    conda-forge
freetype                  2.10.4               h0708190_1    conda-forge
giflib                    5.2.1                h36c2ea0_2    conda-forge
gsl                       2.7                  he838d99_0    conda-forge
gwdatafind                1.1.1              pyhd8ed1ab_0    conda-forge
gwosc                     0.6.1              pyhd8ed1ab_0    conda-forge
gwpy                      2.1.3              pyhd8ed1ab_0    conda-forge
h5py                      3.6.0           nompi_py310he751f51_100    conda-forge
hdf5                      1.12.1          nompi_h2386368_104    conda-forge
icu                       70.1                 h27087fc_0    conda-forge
idna                      3.3                pyhd8ed1ab_0    conda-forge
igwn-auth-utils           0.2.2              pyhd8ed1ab_0    conda-forge
importlib-metadata        4.11.4          py310hff52083_0    conda-forge
ipykernel                 6.15.0                   pypi_0    pypi
ipython                   8.4.0                    pypi_0    pypi
jedi                      0.18.1                   pypi_0    pypi
jpeg                      9e                   h166bdaf_1    conda-forge
jupyter-client            7.3.4                    pypi_0    pypi
jupyter-core              4.10.0                   pypi_0    pypi
keyutils                  1.6.1                h166bdaf_0    conda-forge
kiwisolver                1.4.3           py310hbf28c38_0    conda-forge
krb5                      1.19.3               h3790be6_0    conda-forge
lalsuite                  7.5                      pypi_0    pypi
lcms2                     2.12                 hddcbb42_0    conda-forge
ld_impl_linux-64          2.36.1               hea4e1c9_2    conda-forge
ldas-tools-al             2.6.7                h6e2fe03_1    conda-forge
ldas-tools-framecpp       2.8.2                hf5a2e36_0    conda-forge
lerc                      3.0                  h9c3ff4c_0    conda-forge
libblas                   3.9.0           15_linux64_openblas    conda-forge
libbrotlicommon           1.0.9                h166bdaf_7    conda-forge
libbrotlidec              1.0.9                h166bdaf_7    conda-forge
libbrotlienc              1.0.9                h166bdaf_7    conda-forge
libcblas                  3.9.0           15_linux64_openblas    conda-forge
libcurl                   7.83.1               h7bff187_0    conda-forge
libdeflate                1.12                 h166bdaf_0    conda-forge
libedit                   3.1.20191231         he28a2e2_2    conda-forge
libev                     4.33                 h516909a_1    conda-forge
libffi                    3.4.2                h7f98852_5    conda-forge
libframel                 8.42.1               h166bdaf_0    conda-forge
libgcc-ng                 12.1.0              h8d9b700_16    conda-forge
libgfortran-ng            12.1.0              h69a702a_16    conda-forge
libgfortran5              12.1.0              hdcd56e2_16    conda-forge
libgomp                   12.1.0              h8d9b700_16    conda-forge
liblal                    7.1.7           fftw_h8377b92_100    conda-forge
liblalframe               2.0.0                h166bdaf_0    conda-forge
liblapack                 3.9.0           15_linux64_openblas    conda-forge
libnghttp2                1.47.0               h727a467_0    conda-forge
libnsl                    2.0.0                h7f98852_0    conda-forge
libopenblas               0.3.20          pthreads_h78a6416_0    conda-forge
libpng                    1.6.37               h21135ba_2    conda-forge
libssh2                   1.10.0               ha56f1ee_2    conda-forge
libstdcxx-ng              12.1.0              ha89aaad_16    conda-forge
libtiff                   4.4.0                hc85c160_1    conda-forge
libuuid                   2.32.1            h7f98852_1000    conda-forge
libwebp                   1.2.2                h3452ae3_0    conda-forge
libwebp-base              1.2.2                h7f98852_1    conda-forge
libxcb                    1.13              h7f98852_1004    conda-forge
libzlib                   1.2.12               h166bdaf_1    conda-forge
ligo-segments             1.4.0           py310h6acc77f_2    conda-forge
ligotimegps               2.0.1                      py_0    conda-forge
lscsoft-glue              3.0.1                    pypi_0    pypi
lz4-c                     1.9.3                h9c3ff4c_1    conda-forge
matplotlib-base           3.5.2           py310h5701ce4_0    conda-forge
matplotlib-inline         0.1.3                    pypi_0    pypi
munkres                   1.1.4              pyh9f0ad1d_0    conda-forge
ncurses                   6.3                  h27087fc_1    conda-forge
nest-asyncio              1.5.5                    pypi_0    pypi
numpy                     1.23.0          py310h53a5b5f_0    conda-forge
openjpeg                  2.4.0                hb52868f_1    conda-forge
openssl                   1.1.1p               h166bdaf_0    conda-forge
packaging                 21.3               pyhd8ed1ab_0    conda-forge
parso                     0.8.3                    pypi_0    pypi
pexpect                   4.8.0                    pypi_0    pypi
pickleshare               0.7.5                    pypi_0    pypi
pillow                    9.1.1           py310he619898_1    conda-forge
pip                       22.1.2             pyhd8ed1ab_0    conda-forge
prompt-toolkit            3.0.29                   pypi_0    pypi
psutil                    5.9.1                    pypi_0    pypi
pthread-stubs             0.4               h36c2ea0_1001    conda-forge
ptyprocess                0.7.0                    pypi_0    pypi
pure-eval                 0.2.2                    pypi_0    pypi
pycparser                 2.21               pyhd8ed1ab_0    conda-forge
pyerfa                    2.0.0.1         py310hde88566_2    conda-forge
pygments                  2.12.0                   pypi_0    pypi
pyjwt                     2.4.0              pyhd8ed1ab_0    conda-forge
pyopenssl                 22.0.0             pyhd8ed1ab_0    conda-forge
pyparsing                 3.0.9              pyhd8ed1ab_0    conda-forge
pyrxp                     3.0.1                    pypi_0    pypi
pysocks                   1.7.1           py310hff52083_5    conda-forge
python                    3.10.5          h582c2e5_0_cpython    conda-forge
python-dateutil           2.8.2              pyhd8ed1ab_0    conda-forge
python-lal                7.1.7           fftw_py310h0cc0c6b_100    conda-forge
python-lalframe           2.0.0           py310hde88566_0    conda-forge
python-ldas-tools-al      2.6.10          py310hbf28c38_0    conda-forge
python-ldas-tools-framecpp 2.6.13          py310h769672d_0    conda-forge
python_abi                3.10                    2_cp310    conda-forge
pyyaml                    6.0             py310h5764c6d_4    conda-forge
pyzmq                     23.2.0                   pypi_0    pypi
readline                  8.1.2                h0f457ee_0    conda-forge
requests                  2.28.0             pyhd8ed1ab_0    conda-forge
safe-netrc                1.0.0                      py_0    conda-forge
scipy                     1.8.1           py310h7612f91_0    conda-forge
scitokens                 1.7.0              pyhd8ed1ab_0    conda-forge
setuptools                62.6.0          py310hff52083_0    conda-forge
six                       1.16.0             pyh6c4a22f_0    conda-forge
sqlite                    3.38.5               h4ff8645_0    conda-forge
stack-data                0.3.0                    pypi_0    pypi
tk                        8.6.12               h27826a3_0    conda-forge
tornado                   6.1                      pypi_0    pypi
tqdm                      4.64.0             pyhd8ed1ab_0    conda-forge
traitlets                 5.3.0                    pypi_0    pypi
tzdata                    2022a                h191b570_0    conda-forge
unicodedata2              14.0.0          py310h5764c6d_1    conda-forge
urllib3                   1.26.9             pyhd8ed1ab_0    conda-forge
wcwidth                   0.2.5                    pypi_0    pypi
wheel                     0.37.1             pyhd8ed1ab_0    conda-forge
xorg-libxau               1.0.9                h7f98852_0    conda-forge
xorg-libxdmcp             1.1.3                h7f98852_0    conda-forge
xz                        5.2.5                h516909a_1    conda-forge
yaml                      0.2.5                h7f98852_2    conda-forge
zipp                      3.8.0              pyhd8ed1ab_0    conda-forge
zlib                      1.2.12               h166bdaf_1    conda-forge
zstd                      1.5.2                h8a70e8d_1    conda-forge
